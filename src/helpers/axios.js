import axios from 'axios';

export default axios.create({
    baseURL: 'https://cr7collections.ml/',
    headers: {
        'content-type': 'multipart/form-data'
    }
});