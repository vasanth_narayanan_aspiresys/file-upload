import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import LinearProgress from '@material-ui/core/LinearProgress';
import Moment from 'moment';

import axios from './helpers/axios';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  input: {
    display: 'none',
  },
  table: {
    minWidth: 650,
  }
}));

const App = () => {
  const classes = useStyles();

  const [name, setName] = useState("");
  const [storageDatas, setStorageDatas] = useState([]);
  const [progress, setProgress] = useState(false);

  const handleSubmit = (e) => {
    e.preventDefault();
    const data = new FormData();
    data.append('file', name)
    setProgress(true);

    axios.post('api/upload', data, {
      onUploadProgress: progressEvent => {
        //console.log("upload progress " + Math.round((progressEvent.loaded / progressEvent.total)*100) + "%");
      }
    })
    .then(res => {
      setProgress(false);
      getFiles();
    });
  }

  useEffect(() => {
    getFiles();
  }, []);

  const getFiles = () => {
    axios.get('api/view').then(res => {
      setStorageDatas(res.data);
    })
    .catch(e => console.log(e));
  }

  return (
    <div>
    <Container maxWidth="lg">
    <Container maxWidth="sm">
    <form onSubmit={handleSubmit}>
    <div className={classes.root}>
      <input
        accept="image/*"
        className={classes.input}
        id="contained-button-file"
        multiple
        type="file"
        name="file"
        onChange={(e) => {setName(e.target.files[0])}}
      />
      <label htmlFor="contained-button-file">
        <Button variant="contained" color="primary" component="span">
          Choose File
        </Button>
      </label>
      <Button variant="contained" color="secondary" type="submit">
        Upload File
      </Button>
    </div>
    </form>
    {progress && <LinearProgress />}
    </Container>
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell align="center">#</TableCell>
            <TableCell align="left">File Name</TableCell>
            <TableCell align="center">Created At</TableCell>
            <TableCell align="center">Actions</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
        {storageDatas.map((storateData, index) => 
          <TableRow key={storateData.name}>
            <TableCell align="center">{index+1}</TableCell>
            <TableCell align="left">{storateData.name}</TableCell>
            <TableCell align="center">{Moment(storateData.createdAt).format('DD-MM-YYYY')} </TableCell>
            <TableCell align="center"><a href={storateData.publicUrl} rel="noopener noreferrer" target="_blank">View</a></TableCell>
          </TableRow>
        )}
        </TableBody>
      </Table>
    </TableContainer>
    </Container>
    </div>
  );
}

export default App;
